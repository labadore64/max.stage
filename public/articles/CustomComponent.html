﻿<!DOCTYPE html>
<!--[if IE]><![endif]-->
<html>
  
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Custom Components </title>
    <meta name="viewport" content="width=device-width">
    <meta name="title" content="Custom Components ">
    <meta name="generator" content="docfx 2.48.1.0">
    
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" href="../styles/docfx.vendor.css">
    <link rel="stylesheet" href="../styles/docfx.css">
    <link rel="stylesheet" href="../styles/main.css">
    <meta property="docfx:navrel" content="../toc.html">
    <meta property="docfx:tocrel" content="toc.html">
    
    
    
  </head>
  <body data-spy="scroll" data-target="#affix" data-offset="120">
    <div id="wrapper">
      <header>
        
        <nav id="autocollapse" class="navbar navbar-inverse ng-scope" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              
              <a class="navbar-brand" href="../index.html">
                <img id="logo" class="svg" src="../logo.svg" alt="">
              </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar">
              <form class="navbar-form navbar-right" role="search" id="search">
                <div class="form-group">
                  <input type="text" class="form-control" id="search-query" placeholder="Search" autocomplete="off">
                </div>
              </form>
            </div>
          </div>
        </nav>
        
        <div class="subnav navbar navbar-default">
          <div class="container hide-when-search" id="breadcrumb">
            <ul class="breadcrumb">
              <li></li>
            </ul>
          </div>
        </div>
      </header>
      <div role="main" class="container body-content hide-when-search">
        
        <div class="sidenav hide-when-search">
          <a class="btn toc-toggle collapse" data-toggle="collapse" href="#sidetoggle" aria-expanded="false" aria-controls="sidetoggle">Show / Hide Table of Contents</a>
          <div class="sidetoggle collapse" id="sidetoggle">
            <div id="sidetoc"></div>
          </div>
        </div>
        <div class="article row grid-right">
          <div class="col-md-10">
            <article class="content wrap" id="_content" data-uid="">
<h1 id="custom-components">Custom Components</h1>

<p>You can create your own custom components on layers through implementing the <code>ILayerComponent</code> interface.</p>
<h2 id="inheritance">Inheritance</h2>
<p>All components must implement the <code>ILayerComponent</code> interface. While you can use this interface directly, it is easier to inherit one of the abstract component classes, such as <code>LayerComponent</code>, to build your component off of. This tutorial will assume you are inheriting the class <code>LayerComponent</code>.</p>
<p>When inheriting <code>LayerComponent</code>, the following serialization methods should be overriden:</p>
<ul>
<li><code>CreateInstance</code></li>
<li><code>Deserialize</code></li>
<li><code>DoSerialize</code></li>
</ul>
<p>All classes inheriting the <code>LayerComponent</code> also require an empty constructor.</p>
<h2 id="flow">Flow</h2>
<p>All layer components have the three stages defined:</p>
<ul>
<li><code>Initialize()</code> - Called when the layer component is created. Is used to create the start state of the component.</li>
<li><code>Update()</code> - Called once every time the Stage Controller method <code>Update</code> is called.</li>
<li><code>Dispose()</code> - Called when the layer component is destroyed. Is used to free all resources used by the component.</li>
</ul>
<p>You can use the defaults for these methods, for example, if your object does not need any resources to initialize or be disposed of, you can ignore overriding these methods.</p>
<p>Keep in mind that the components will be updated in the order that they are in the list of the layer, and layers are updated in the order that they are stored in their list.</p>
<p>Note: In addition, there are also three methods that start these methods and apply different conditions to them:</p>
<ul>
<li><code>DoInitialize()</code></li>
<li><code>DoUpdate()</code></li>
<li><code>DoDispose()</code></li>
</ul>
<p>These cannot be overriden, however, if you want to call <code>Initialize()</code>, <code>Update()</code> or <code>Dispose()</code>, you should call the <code>Do</code> version of that method to make sure those conditions are applied.</p>
<h2 id="example">Example</h2>
<p>Let's say that we want to build a component that has a <code>Vector2</code> position and an <code>int</code> quantity. We can define the class like this:</p>
<pre><code>public class TestComponent : LayerComponent
{
  
  public Vector2 Position { get; set; } = Vector2.Zero;
  public int Quantity { get; set; } = 0;

  // empty constructor
  public TestComponent()
  {
  }

  // Creates an instance of this object from the data.
  public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
  {
    IMaxSerializable obj = new TestComponent();
    obj.Deserialize(Data);
    return obj;
  }

  // Deserializes this object.
  public override void Deserialize(MaxSerializedObject Data)
  {
    Position = Data.GetVector2(&quot;Position&quot;);
    Quantity = Data.GetInt(&quot;Quantity&quot;);
  }

  // Serializes this object.
  protected override MaxSerializedObject DoSerialize(MaxSerializedObject Data)
  {
    Data.AddVector2(&quot;Position&quot;, Position);
    Data.AddInt(&quot;Quantity&quot;, Quantity);
    return Data;
  }
}
</code></pre>
<p>This will work, but it won't actually do anything by itself. Let's say that we want to update the int value every update cycle. We will have to override the <code>Update()</code> method to do this:</p>
<pre><code>// this will increase the value of Quantity until
// it reaches 100, then it will reset the counter
public override void Update(){
  Quantity++;
  if(Quantity &gt; 100){
    Quantity = 0;
  }
}
</code></pre>
<p>You can also override the other two flow methods, for example if you are using <code>max.sound</code> and <code>SoundEffect</code>:</p>
<pre><code>// the SoundEffect variable
SoundEffect sfx;

// ...

// sets a sound effect
public override void Initialize(StageController StageController){
  // you should set the parent
  Parent = StageController;

  // sets and plays the sound effect
  sfx = SoundController.GetSoundEffect(&quot;TestSound&quot;).Copy();
  sfx.IsLooped = true;
  sfx.Play();
}

// disposes of the sound effect resources
public override void Dispose(){
  if(sfx != null){
    sfx.Dispose();
  }
</code></pre>
</article>
          </div>
          
          <div class="hidden-sm col-md-2" role="complementary">
            <div class="sideaffix">
              <div class="contribution">
                <ul class="nav">
                </ul>
              </div>
              <nav class="bs-docs-sidebar hidden-print hidden-xs hidden-sm affix" id="affix">
              <!-- <p><a class="back-to-top" href="#top">Back to top</a><p> -->
              </nav>
            </div>
          </div>
        </div>
      </div>
      
      <footer>
        <div class="grad-bottom"></div>
        <div class="footer">
          <div class="container">
            <span class="pull-right">
              <a href="#top">Back to top</a>
            </span>
            
            <span>Generated by <strong>DocFX</strong></span>
          </div>
        </div>
      </footer>
    </div>
    
    <script type="text/javascript" src="../styles/docfx.vendor.js"></script>
    <script type="text/javascript" src="../styles/docfx.js"></script>
    <script type="text/javascript" src="../styles/main.js"></script>
  </body>
</html>

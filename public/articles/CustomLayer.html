﻿<!DOCTYPE html>
<!--[if IE]><![endif]-->
<html>
  
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Custom Layers </title>
    <meta name="viewport" content="width=device-width">
    <meta name="title" content="Custom Layers ">
    <meta name="generator" content="docfx 2.48.1.0">
    
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" href="../styles/docfx.vendor.css">
    <link rel="stylesheet" href="../styles/docfx.css">
    <link rel="stylesheet" href="../styles/main.css">
    <meta property="docfx:navrel" content="../toc.html">
    <meta property="docfx:tocrel" content="toc.html">
    
    
    
  </head>
  <body data-spy="scroll" data-target="#affix" data-offset="120">
    <div id="wrapper">
      <header>
        
        <nav id="autocollapse" class="navbar navbar-inverse ng-scope" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              
              <a class="navbar-brand" href="../index.html">
                <img id="logo" class="svg" src="../logo.svg" alt="">
              </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar">
              <form class="navbar-form navbar-right" role="search" id="search">
                <div class="form-group">
                  <input type="text" class="form-control" id="search-query" placeholder="Search" autocomplete="off">
                </div>
              </form>
            </div>
          </div>
        </nav>
        
        <div class="subnav navbar navbar-default">
          <div class="container hide-when-search" id="breadcrumb">
            <ul class="breadcrumb">
              <li></li>
            </ul>
          </div>
        </div>
      </header>
      <div role="main" class="container body-content hide-when-search">
        
        <div class="sidenav hide-when-search">
          <a class="btn toc-toggle collapse" data-toggle="collapse" href="#sidetoggle" aria-expanded="false" aria-controls="sidetoggle">Show / Hide Table of Contents</a>
          <div class="sidetoggle collapse" id="sidetoggle">
            <div id="sidetoc"></div>
          </div>
        </div>
        <div class="article row grid-right">
          <div class="col-md-10">
            <article class="content wrap" id="_content" data-uid="">
<h1 id="custom-layers">Custom Layers</h1>

<h2 id="inheritance">Inheritance</h2>
<p>All layers must implement the <code>IStagelayer</code> interface. While you can use this interface directly, it is easier to inherit one of the abstract layer classes, such as <code>Layer</code>, to build your layer off of. This tutorial will assume you are inheriting the class <code>Layer</code>.</p>
<h3 id="layer">Layer</h3>
<p>When inheriting <code>Layer</code>, the following methods must be overriden:</p>
<ul>
<li><code>Components</code></li>
<li><code>AddComponent()</code></li>
<li><code>RemoveComponent()</code></li>
<li><code>Clear()</code></li>
<li><code>ClearAll()</code></li>
<li><code>CreateInstance()</code></li>
</ul>
<p>In addition, all classes inheriting <code>Layer</code> must have a default constructor.</p>
<h3 id="componentlayercontrollerlayer">ComponentLayer/ControllerLayer</h3>
<p>Many layer types can also be built on the abstract classes <code>ComponentLayer</code> or <code>ControllerLayer</code>. This may be easier since many of the layer manipulation functions are already taken care for you.</p>
<p>Use <code>ComponentLayer</code> if:</p>
<ul>
<li>You are managing more than one component (such as interactable objects or collisions)</li>
<li>You need to remove/add components dynamically.</li>
</ul>
<p>Use <code>ControllerLayer</code> if:</p>
<ul>
<li>You are managing only one component (such as a special script controller)</li>
<li>The only time you will remove the component is if the stage changes, if at all.</li>
</ul>
<p>When inheriting <code>ComponentLayer</code> or <code>ControllerLayer</code>, the following method must be overriden:</p>
<ul>
<li><code>CreateInstance()</code></li>
</ul>
<p>In addition, with the ControllerLayer, the property <code>Controller</code> must be assigned to a controller that implements <code>ILayerComponent</code>, ideally in its constructor.</p>
<h2 id="flow">Flow</h2>
<p>All layers have the three stages defined:</p>
<ul>
<li><code>Initialize()</code> - Called when the layer is created. Is used to create the start state of the layer.</li>
<li><code>Update()</code> - Called once every time the Stage Controller method <code>Update</code> is called.</li>
<li><code>Dispose()</code> - Called when the layer is destroyed. Is used to free all resources used by the layer.</li>
</ul>
<p>You can use the defaults for these methods, for example, if your object does not need any resources to initialize or be disposed of, you can ignore overriding these methods.</p>
<p>Keep in mind that layers are updated in the order that they are stored in their parent <code>StageController</code> or <code>ContainerLayer</code>.</p>
<p>Note: In addition, there are also three methods that start these methods and apply different conditions to them:</p>
<ul>
<li><code>DoInitialize()</code></li>
<li><code>DoUpdate()</code></li>
<li><code>DoDispose()</code></li>
</ul>
<p>These cannot be overriden, however, if you want to call <code>Initialize()</code>, <code>Update()</code> or <code>Dispose()</code>, you should call the <code>Do</code> version of that method to make sure those conditions are applied.</p>
<h2 id="examples">Examples</h2>
<h3 id="inheriting-layer">Inheriting Layer</h3>
<p>This is a simple sample layer class. It functionally resembles a class inheriting <code>ComponentLayer</code>.</p>
<pre><code>public class TestLayer : Layer
{
    // The component list.
    public override List&lt;ILayerComponent&gt; Components { get; protected set; } = new List&lt;ILayerComponent&gt;();

    // default constructor
    public TestLayer(){
    
	}

    // Clears all non-persistent components
    public override void Clear()
    {
        for(int i = 0; i &lt; Components.Count; i++)
        {
            if (!Components[i].Persistent)
            {
                RemoveComponent(Components[i]);
                i--;
            }
        }
    }

    // Clears all components
    public override void ClearAll()
    {
        for (int i = 0; i &lt; Components.Count; i++)
        {
            if (!Components[i].Persistent)
            {
                Components[i].Dispose();
            }
        }

        Components.Clear();
    }

    // Create an instance of this type and deserialize it with the Data provided
    public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
    {
        IMaxSerializable obj = new TestLayer();
        obj.Deserialize(Data);
        return obj;
    }

    // Add a component
    public override void AddComponent(ILayerComponent Item)
    {
        Item.DoInitialize(this);
        Components.Add(Item);
    }

    // Remove a component
    public override void RemoveComponent(ILayerComponent Item)
    {
        Item.Dispose();
        Components.Remove(Item);
    }
}
</code></pre>
<h3 id="inheriting-component-layer">Inheriting Component Layer</h3>
<p>Here is a bare bones inheritance example of ComponentLayer:</p>
<pre><code>public class TestLayer : ComponentLayer
{
    // default constructor
    public TestLayer() : base(){
    
	}

    // Create an instance of this type and deserialize it with the Data provided
    public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
    {
        IMaxSerializable obj = new TestLayer();
        obj.Deserialize(Data);
        return obj;
    }

}
</code></pre>
<p>Note: To reduce the amount of type casting required, you may want to create an additional list that only uses the specific type you're referencing.</p>
<pre><code>public override List&lt;ILayerComponent&gt; Components { get; protected set; } = new List&lt;ILayerComponent&gt;();

public List&lt;CollisionComponent&gt; Collisions
{
    get
    {
        return _collisions;
    }
    set
    {
        _collisions = value;
        Components = new List&lt;ILayerComponent&gt;(_collisions);
    }
}

List&lt;CollisionComponent&gt; _collisions = new List&lt;CollisionComponent&gt;();
</code></pre>
<p>In this example in <code>CollisionLayer</code>, 3 properties/fields are actually used to reference the component list. <code>Components</code> is inherited from <code>Layer</code> and it copies the list of collisions as a <code>List&lt;ILayerComponent&gt;</code> every time the <code>Collisions</code> property is set. To prevent stack overflow errors, the value of <code>Collisions</code> is stored in a private variable, <code>_collisions</code>. This allows the layer to reference the collision objects and their properties directly without excessive casting, increasing efficiency.</p>
<p><a href="https://gitlab.com/labadore64/max.stage/blob/master/src/max/stage/layer/CollisionLayer.cs">View the source</a> for <code>CollisionLayer</code> for more details on this example.</p>
<h3 id="inheriting-controller-layer">Inheriting Controller Layer</h3>
<p>Here is a bare bones inheritance example of <code>ControllerLayer</code>. It is similar to <code>ComponentLayer</code>:</p>
<pre><code>public class TestLayer : ComponentLayer
{
    // default constructor
    public TestLayer() : base(){
        Controller = new TestController();
	}

    // Create an instance of this type and deserialize it with the Data provided
    public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
    {
        IMaxSerializable obj = new TestLayer();
        obj.Deserialize(Data);
        return obj;
    }

}
</code></pre>
<p>Notice that the controller is instantiated in the constructor - <code>ControllerLayer</code> manages only one component - a controller - at a time.</p>
<p><a href="">View the source</a> for <code>ProximityDetectorControllerLayer</code> in <code>max.vision</code> for more details on this example.</p>
</article>
          </div>
          
          <div class="hidden-sm col-md-2" role="complementary">
            <div class="sideaffix">
              <div class="contribution">
                <ul class="nav">
                </ul>
              </div>
              <nav class="bs-docs-sidebar hidden-print hidden-xs hidden-sm affix" id="affix">
              <!-- <p><a class="back-to-top" href="#top">Back to top</a><p> -->
              </nav>
            </div>
          </div>
        </div>
      </div>
      
      <footer>
        <div class="grad-bottom"></div>
        <div class="footer">
          <div class="container">
            <span class="pull-right">
              <a href="#top">Back to top</a>
            </span>
            
            <span>Generated by <strong>DocFX</strong></span>
          </div>
        </div>
      </footer>
    </div>
    
    <script type="text/javascript" src="../styles/docfx.vendor.js"></script>
    <script type="text/javascript" src="../styles/docfx.js"></script>
    <script type="text/javascript" src="../styles/main.js"></script>
  </body>
</html>

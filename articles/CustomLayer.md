# Custom Layers

## Inheritance

All layers must implement the ``IStagelayer`` interface. While you can use this interface directly, it is easier to inherit one of the abstract layer classes, such as ``Layer``, to build your layer off of. This tutorial will assume you are inheriting the class ``Layer``.

### Layer

When inheriting ``Layer``, the following methods must be overriden:

* ``Components`` 
* ``AddComponent()``
* ``RemoveComponent()``
* ``Clear()``
* ``ClearAll()``
* ``CreateInstance()``

In addition, all classes inheriting ``Layer`` must have a default constructor.

### ComponentLayer/ControllerLayer

Many layer types can also be built on the abstract classes ``ComponentLayer`` or ``ControllerLayer``. This may be easier since many of the layer manipulation functions are already taken care for you.

Use ``ComponentLayer`` if:
* You are managing more than one component (such as interactable objects or collisions)
* You need to remove/add components dynamically.

Use ``ControllerLayer`` if:
* You are managing only one component (such as a special script controller)
* The only time you will remove the component is if the stage changes, if at all.

When inheriting ``ComponentLayer`` or ``ControllerLayer``, the following method must be overriden:

* ``CreateInstance()``

In addition, with the ControllerLayer, the property ``Controller`` must be assigned to a controller that implements ``ILayerComponent``, ideally in its constructor.

## Flow

All layers have the three stages defined:

* ``Initialize()`` - Called when the layer is created. Is used to create the start state of the layer.
* ``Update()`` - Called once every time the Stage Controller method ``Update`` is called.
* ``Dispose()`` - Called when the layer is destroyed. Is used to free all resources used by the layer.

You can use the defaults for these methods, for example, if your object does not need any resources to initialize or be disposed of, you can ignore overriding these methods.

Keep in mind that layers are updated in the order that they are stored in their parent ``StageController`` or ``ContainerLayer``.

Note: In addition, there are also three methods that start these methods and apply different conditions to them:
* ``DoInitialize()``
* ``DoUpdate()``
* ``DoDispose()``

These cannot be overriden, however, if you want to call ``Initialize()``, ``Update()`` or ``Dispose()``, you should call the ``Do`` version of that method to make sure those conditions are applied.

## Examples

### Inheriting Layer

This is a simple sample layer class. It functionally resembles a class inheriting ``ComponentLayer``.

```
public class TestLayer : Layer
{
    // The component list.
    public override List<ILayerComponent> Components { get; protected set; } = new List<ILayerComponent>();

    // default constructor
    public TestLayer(){
    
	}

    // Clears all non-persistent components
    public override void Clear()
    {
        for(int i = 0; i < Components.Count; i++)
        {
            if (!Components[i].Persistent)
            {
                RemoveComponent(Components[i]);
                i--;
            }
        }
    }

    // Clears all components
    public override void ClearAll()
    {
        for (int i = 0; i < Components.Count; i++)
        {
            if (!Components[i].Persistent)
            {
                Components[i].Dispose();
            }
        }

        Components.Clear();
    }

    // Create an instance of this type and deserialize it with the Data provided
    public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
    {
        IMaxSerializable obj = new TestLayer();
        obj.Deserialize(Data);
        return obj;
    }

    // Add a component
    public override void AddComponent(ILayerComponent Item)
    {
        Item.DoInitialize(this);
        Components.Add(Item);
    }

    // Remove a component
    public override void RemoveComponent(ILayerComponent Item)
    {
        Item.Dispose();
        Components.Remove(Item);
    }
}
```

### Inheriting Component Layer

Here is a bare bones inheritance example of ComponentLayer:

```
public class TestLayer : ComponentLayer
{
    // default constructor
    public TestLayer() : base(){
    
	}

    // Create an instance of this type and deserialize it with the Data provided
    public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
    {
        IMaxSerializable obj = new TestLayer();
        obj.Deserialize(Data);
        return obj;
    }

}
```

Note: To reduce the amount of type casting required, you may want to create an additional list that only uses the specific type you're referencing.

```
public override List<ILayerComponent> Components { get; protected set; } = new List<ILayerComponent>();

public List<CollisionComponent> Collisions
{
    get
    {
        return _collisions;
    }
    set
    {
        _collisions = value;
        Components = new List<ILayerComponent>(_collisions);
    }
}

List<CollisionComponent> _collisions = new List<CollisionComponent>();
```

In this example in ``CollisionLayer``, 3 properties/fields are actually used to reference the component list. ``Components`` is inherited from ``Layer`` and it copies the list of collisions as a ``List<ILayerComponent>`` every time the ``Collisions`` property is set. To prevent stack overflow errors, the value of ``Collisions`` is stored in a private variable, ``_collisions``. This allows the layer to reference the collision objects and their properties directly without excessive casting, increasing efficiency.

[View the source](https://gitlab.com/labadore64/max.stage/blob/master/src/max/stage/layer/CollisionLayer.cs) for ``CollisionLayer`` for more details on this example.

### Inheriting Controller Layer

Here is a bare bones inheritance example of ``ControllerLayer``. It is similar to ``ComponentLayer``:

```
public class TestLayer : ComponentLayer
{
    // default constructor
    public TestLayer() : base(){
        Controller = new TestController();
	}

    // Create an instance of this type and deserialize it with the Data provided
    public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
    {
        IMaxSerializable obj = new TestLayer();
        obj.Deserialize(Data);
        return obj;
    }

}
```

Notice that the controller is instantiated in the constructor - ``ControllerLayer`` manages only one component - a controller - at a time.

[View the source]() for ``ProximityDetectorControllerLayer`` in ``max.vision`` for more details on this example.
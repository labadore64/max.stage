# Layers

Layers are sets of components with similar characteristics, such as collision data, tile data, or object data. Every layer should implement the ``IStageLayer`` interface, or use the abstract class ``StageLayer`` as a template.

## Layer behavior

Every layer inherits several control behaviors from the ``ILayerComponent`` interface. These behaviors help control the layer and its child components' activity across its life cycle. All control methods can be overridden to accomidate custom functionality.

### Initialize(StageController)

When you add a layer to the Stage Controller, the Initialize method is executed and passes the calling Stage Controller's instance as its argument. This allows you to set the ``Parent`` variable among other things. In the ``StageLayer`` class, the ``Initialize`` method already does this for you.

You should call ``base.Initialize(Layer);`` in your method in most cases as well so that the ``Parent`` variable is set. If inheriting ``StageLayer``, the method is virtual, so overriding is not necessary.

### Update()

This method updates the state of the layer and its children every time the StageController's ``Update()`` method is called, usually once per game cycle. Code involving how the layer updates its components should go here.

If inheriting ``StageLayer``, the method is virtual, so overriding is not necessary.

### Dispose()

This method executes when the layer is removed from the Stage Controller. It should be used to dispose of all its children, as well as remove any possible other unreleased resources.

You should call ``base.Dispose();`` in your method in most cases as well so that the ``Disposed`` variable is set. If inheriting ``StageLayer``, the method is virtual, so overriding is not necessary.

### Component Serialization

The ``Serialize()`` and ``Deserialize()`` methods serialize and deserialize the elements of the layer - this allows you to save and load layer states. These methods work with the ``MaxSerializedObject`` class, and thus are compatible with the ``max.serialize`` and ``max.io`` libraries, allowing you to easily output layer data to a file and read files as layers.

### Layer Properties

There are several properties to control how layers interact with other behaviors in certain conditions.

* ``Persistent`` - If true, this layer will not be removed by calling ``Clear()``. You must call ``ClearAll()`` or remove the layer directly. This allows some layers to be persistent between changing stages.
* ``Serializable`` - If true, this layer is serialized when the parent layer's ``Serialize()`` method is called.
* ``Initialized`` - If true, the layer has been successfully initialized. This also prevents initialization occuring more than once.
* ``Disposed`` - If true, the layer has been successfully disposed.
* ``Active`` - If true, the layer is updated this cycle.
* ``Parent`` - The parent layer.
* ``StageController`` - The top Stage Controller.
* ``ID`` - The unique ID name of the layer.
* ``Components`` - The list of components contained in this layer. In ``ComponentLayer``, this list will have more than one value, but in ``ControllerLayer`` it always returns a list with one value as the controller.
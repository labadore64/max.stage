# Stage Structure

Stages allow you to "stage" objects so that they can be used by the game, such as collisions, objects on a map, or even controllers.

Stages are built with layers, and each layer is built with components - these components are the individual objects that you interact with on a stage.

When you update the Stage Controller, all of the layers are updated in order of their index, starting from 0, and update all of their components in order of their index, starting from 0. The following diagram describes both the structure and the execute order of the objects:

```
- Stage
  - Layer 1
    - Collision 1
	- Collision 2
	...
  - Layer 2
    - Object 1
	- Object 2
	...
  - Layer ...
```

For more in depth details, view the following pages:

* Parts
	* [Components](Components.md)
	* [Controllers](Controllers.md)
	* [Layers](Layers.md)
	* [Types](Types.md)
* Tutorial
	* [Building Stages](BuildStage.md)
	* [Saving/Loading Stages](SaveLoad.md)
	* [Custom Components](CustomComponent.md)
	* [Custom Layers](CustomLayer.md)
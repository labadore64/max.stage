# Using max.stage

``max.stage`` is a MaxLib Game Library that allows you to place game objects on a stage.

* [Installation](Installation.md)
* [Stage Structure](StageStructure.md)
	* Parts
		* [Components](Components.md)
		* [Controllers](Controllers.md)
		* [Layers](Layers.md)
		* [Types](Types.md)
	* Tutorial
		* [Building Stages](BuildStage.md)
		* [Saving/Loading Stages](SaveLoad.md)
		* [Custom Components](CustomComponent.md)
		* [Custom Layers](CustomLayer.md)


## Max Dependencies:

* [max.geometry](https://labadore64.gitlab.io/max.geometry/)
* [max.serialize](https://labadore64.gitlab.io/max.serialize/)
* [max.sound](https://labadore64.gitlab.io/max.sound/)
* [max.util](https://labadore64.gitlab.io/max.util/)
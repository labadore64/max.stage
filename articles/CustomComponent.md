# Custom Components

You can create your own custom components on layers through implementing the ``ILayerComponent`` interface.

## Inheritance

All components must implement the ``ILayerComponent`` interface. While you can use this interface directly, it is easier to inherit one of the abstract component classes, such as ``LayerComponent``, to build your component off of. This tutorial will assume you are inheriting the class ``LayerComponent``.

When inheriting ``LayerComponent``, the following serialization methods should be overriden:

* ``CreateInstance``
* ``Deserialize``
* ``DoSerialize``

All classes inheriting the ``LayerComponent`` also require an empty constructor.

## Flow

All layer components have the three stages defined:

* ``Initialize()`` - Called when the layer component is created. Is used to create the start state of the component.
* ``Update()`` - Called once every time the Stage Controller method ``Update`` is called.
* ``Dispose()`` - Called when the layer component is destroyed. Is used to free all resources used by the component.

You can use the defaults for these methods, for example, if your object does not need any resources to initialize or be disposed of, you can ignore overriding these methods.

Keep in mind that the components will be updated in the order that they are in the list of the layer, and layers are updated in the order that they are stored in their list.

Note: In addition, there are also three methods that start these methods and apply different conditions to them:
* ``DoInitialize()``
* ``DoUpdate()``
* ``DoDispose()``

These cannot be overriden, however, if you want to call ``Initialize()``, ``Update()`` or ``Dispose()``, you should call the ``Do`` version of that method to make sure those conditions are applied.

## Example

Let's say that we want to build a component that has a ``Vector2`` position and an ``int`` quantity. We can define the class like this:

```
public class TestComponent : LayerComponent
{
  
  public Vector2 Position { get; set; } = Vector2.Zero;
  public int Quantity { get; set; } = 0;

  // empty constructor
  public TestComponent()
  {
  }

  // Creates an instance of this object from the data.
  public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
  {
    IMaxSerializable obj = new TestComponent();
    obj.Deserialize(Data);
    return obj;
  }

  // Deserializes this object.
  public override void Deserialize(MaxSerializedObject Data)
  {
    Position = Data.GetVector2("Position");
    Quantity = Data.GetInt("Quantity");
  }

  // Serializes this object.
  protected override MaxSerializedObject DoSerialize(MaxSerializedObject Data)
  {
    Data.AddVector2("Position", Position);
    Data.AddInt("Quantity", Quantity);
    return Data;
  }
}
```

This will work, but it won't actually do anything by itself. Let's say that we want to update the int value every update cycle. We will have to override the ``Update()`` method to do this:

```
// this will increase the value of Quantity until
// it reaches 100, then it will reset the counter
public override void Update(){
  Quantity++;
  if(Quantity > 100){
    Quantity = 0;
  }
}
```

You can also override the other two flow methods, for example if you are using ``max.sound`` and ``SoundEffect``:

```
// the SoundEffect variable
SoundEffect sfx;

// ...

// sets a sound effect
public override void Initialize(StageController StageController){
  // you should set the parent
  Parent = StageController;

  // sets and plays the sound effect
  sfx = SoundController.GetSoundEffect("TestSound").Copy();
  sfx.IsLooped = true;
  sfx.Play();
}

// disposes of the sound effect resources
public override void Dispose(){
  if(sfx != null){
    sfx.Dispose();
  }
```
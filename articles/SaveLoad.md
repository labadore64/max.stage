# Saving/Loading Stages

You can use the serialization methods to save and load the data in stages.

## Saving

To save all the layers, you can call the StageController's ``Serialize()`` method to return a ``MaxSerializableObject`` that you can save to a file using ``max.io``.

```
// serializes stage data into a serializable object
MaxSerializableObject data = StageController.Serialize();
```

You can also save layers in a similar fashion.

```
// serializes stage data into a serializable object
MaxSerializableObject data = StageController.GetLayer("Collisions").Serialize();
```

If you do not want a specific layer or component to serialize, you should set that layer's or component's ``Serializable`` property to ``false``.

```
// Creates a new layer.
CollisionLayer layer = new CollisionLayer("Collisions");

// makes it so that this layer will not be serialized.
layer.Serializable = false;
```

## Loading

You can load data from files by using ``max.serialize`` to convert text files into ``MaxSerializableObject``. Once you have the serialized object for the Stage Controller, you can just deserialize it.

```
// deserializes the passed data.
StageController.Deserialize(Data);
```

## Further Reading

``MaxSerializableObject`` is a class in [max.serialize](https://labadore64.gitlab.io/max.serialize/). max.serialize is directly compatible with [max.io](https://labadore64.gitlab.io/max.io/) which performs reading/writing operations. Refer to their documentation for more detail on how to perform these tasks.
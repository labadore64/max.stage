# Components

Components are the individual objects that make up stages - these can range from collisions to interactable objects to graphics. Every component should implement the ``ILayerComponent`` interface, or use the abstract class ``LayerComponent`` as a template.

## Component behavior

Every component inherits several control behaviors from the ``ILayerComponent`` interface. These behaviors help control the component's activity across its life cycle. All control methods can be overridden to accomidate custom functionality.

### Initialize(IStageLayer Layer)

When you add a component to a Layer, the Initialize method is executed and passes the calling Layer's instance as its argument. This allows you to set the ``Parent`` variable among other things. In the ``LayerComponent`` class, the ``Initialize`` method already does this for you.

You should call ``base.Initialize(Layer);`` in your method in most cases as well so that the ``Parent`` variable is set. If inheriting ``LayerComponent``, the method is virtual, so overriding is not necessary.

### Update()

This method updates the state of the component every time the StageController's ``Update()`` method is called, usually once per game cycle. Code involving how the component interacts with other components should go here.

If inheriting ``LayerComponent``, the method is virtual, so overriding is not necessary.

### Dispose()

This method executes when the component is removed from the layer. It should be used to release resources that may otherwise cause memory leaks, or other behaviors that should occur when the component is disposed.

You should call ``base.Dispose();`` in your method in most cases as well so that the ``Disposed`` variable is set. If inheriting ``LayerComponent``, the method is virtual, so overriding is not necessary.

### Component Serialization

The ``Serialize()`` and ``Deserialize()`` methods should be implemented in any components you build, since it will allow the StageController to load components to layers from a file.

If you override a component's ``Serialize`` method, you must make sure that you use the following as a template:

```
public override MaxSerializedObject Serialize()
{
    MaxSerializedObject Data = base.Serialize();
    // ...

    return Data;
}
```

This preserves the entry that records the type of object that's being used - without this, deserialization will fail!

### Component Properties

There are several properties to control how components interact with other behaviors in certain conditions.

* ``Persistent`` - If true, this component will not be removed by calling ``Clear()``. You must call ``ClearAll()`` or remove the component directly. This allows some components to be persistent between changing stages. If the parent layer is removed, persistent components will be removed as well.
* ``Serializable`` - If true, this component is serialized when the parent layer's ``Serialize()`` method is called.
* ``Initialized`` - If true, the component has been successfully initialized. This also prevents initialization occuring more than once.
* ``Disposed`` - If true, the component has been successfully disposed.
* ``Active`` - If true, the component is updated this cycle.
* ``Parent`` - The parent layer.
* ``StageController`` - The top Stage Controller.
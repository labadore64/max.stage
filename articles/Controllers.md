# Controllers

Controllers are a special type of ``ILayerComponent`` that inherits the class ``Controller``, and represents a layer that has only one object, such as a player or a controller for environmental effects.

Controllers must override the following serialization methods:

* ``Serialize()``
* ``Deserialize()``
* ``CreateInstance()``

Because controllers are specialized components, they share the same properties and behaviors as them. You can read more [here](Components.md).
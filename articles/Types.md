# Types

There are three major kinds of types used in ``max.stage``: Components, Layers and Controllers.

## Component Types

Components are objects that represent an entity on the stage, such as collision polygons, entities or events. All other types inherit the basic structure of components.

* ``LayerComponent`` is an abstract type that represents a component on a layer. While you can define components by simply implementing ``ILayerComponent``, by inheriting ``LayerComponent`` it will standardize most of the methods.
* ``GeometryComponent`` is an abstract type that represents a component with geometry, such as a collision or hitbox.
* ``CollisionComponent`` is a type that represents a collision polygon on a stage.

## Controller Types

Controllers are components that are used only in classes that inherit ``ControllerLayer`` and represent an object that manages a static functionality on the stage. By default, controllers are persistent and not serializable.

* ``Controller`` is an abstract type that represents a template for a controller, used in classes that inherit ``ControllerLayer``.
* ``Player2DController`` is a type that inherits ``Controller`` that represents the player on the stage. It stores the player's location and updates the sound controller's listener position and orientation as it moves. It also has a dictionary of properties that can be used in classes that inherit ``Player2DController`` or when interacting with other components.

## Layer Types

Layers are groups of components that are updated and disposed of at the same time as a unit, and often work together, like collision maps or controllers. Layers allow these functions to be managed through the Stage Controller programmically.

* ``Layer`` is an abstract type that represents a template for layers. While you can also implement ``IStageLayer`` with your custom layer classes, you can also simply inherit ``Layer`` and achieve most basic functionality.
* ``ComponentLayer`` is an abstract type that manages a collection of components in a single layer, such as items on a stage.
* ``ControllerLayer`` is an abstract type that manages only one component, a Controller, which is used to manage objects that don't have specific instances on the stage, such as a player.
* ``ContainerLayer`` stores other layers in a single layer.
* ``CollisionLayer`` stores a set of ``CollisionComponent`` instances that represents collisions.
* ``Player2DLayer`` is the layer that contains a ``Player2DController`` and acts as the player.

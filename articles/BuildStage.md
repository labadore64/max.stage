# Building Stages

You can build stages easily by using layers with components attached to them.

## Building the Stage

### Initializing

To prepare the stage controller, call it's ``Clear()`` method. All layers that are not persistent will be removed and disposed of. (If you want to also get rid of the persistent layers, call ``ClearAll()``).

```
StageController.Clear();
```

### Layer Types

To insert components, you must add layers.

There are several prepackaged layer types for you to choose from.

The types of layer included with ``max.stage`` are:

* Abstract Types
  * ``ComponentLayer``
    * A layer type that stores a list of components (like objects or events)
  * ``ControllerLayer``
    * A layer type that handles a controller (such as a special functionality used on that stage)
* Concrete Types
  * ``CollisionLayer``
    * A layer type that stores a list of collision data.
  * ``ContainerLayer``
    * A layer type that acts as a container for other layers.

You can also build your own layers by following [this tutorial](CustomLayer.md). 

### Component Types

There are several prepackaged component types for you to choose from.

The types of components included with ``max.stage`` are:

* Abstract Types
  * ``LayerComponent``
    * The abstract component class.
  * ``GeometryComponent``
    * Represents an abstract geometrical component.
* Concrete Types
  * ``CollisionComponent``
    A component that represents collision data as polygons.

You can also build your own layers by following [this tutorial](CustomComponent.md). 

### Adding Layers and Components

As a demonstration, this tutorial is going to create an instance of ``CollisionLayer`` and add some collision data to it.

First, create the new layer. Make sure you also add it to the stage controller.

```
// Creates a new CollisionLayer called "collisions"
CollisionLayer layer = new CollisionLayer("collisions");

// Adds the new layer to the stage controller.
StageController.AddLayer(layer);
```

The layer is now added, but it needs to be populated with components. You can add components directly. This is supported by all layer types.

```
// adds a new collision component with boundary Polygon
layer.AddComponent(new CollisionComponent(polygon));
```

Let's assume that you imported a ``Polygon2D[]`` with polygons representing collision data. ``CollisionLayer`` has a useful method, ``AddComponents()``, that allows to add polygons directly as collision components.

```
// Adds the array polygons to the layer, converting them
// into CollisionComponents
layer.AddComponents(polygons);
```

If you want your layer to persist between stages, you should make sure that you set the layer's ``Persistence`` property.

```
// This will cause this layer to not be unloaded when Clear() 
// called by the Stage Controller.
layer.Persistent = true;
```

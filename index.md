# max.stage

``max.stage`` is a MaxLib Game Library that allows you to place game objects on a stage.

* [Documentation](https://labadore64.gitlab.io/max.stage/)
* [Source](https://gitlab.com/labadore64/max.stage/)

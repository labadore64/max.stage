﻿using max.serialize;
using max.sound;
using max.sound.controller;
using max.stage.layer.template;
using System.Collections.Generic;

namespace max.stage
{
    /// <summary>
    /// This class manages the stage using a list of layers.
    /// </summary>
    public class StageController: IMaxSerializable
    {
        #region Constants
        const string SER_LAYERS = "layers";
        #endregion

        #region Properties

        /// <summary>
        /// Whether or not this component is serialized when the parent's Serialize method is called.
        /// </summary>
        /// <value>True/False</value>
        public bool Serializable { get; set; } = true;

        /// <summary>
        /// The height of the stage. While objects can exist beyond this height, this is used
        /// as a boundary for some operations.
        /// </summary>
        /// <value>Height</value>
        public int Height { get; set; } = 600;
        /// <summary>
        /// The width of the stage. While objects can exist beyond this height, this is used
        /// as a boundary for some operations.
        /// </summary>
        /// <value>Width</value>
        public int Width { get; set; } = 800;

        /// <summary>
        /// The layers of the stage controller.
        /// </summary>
        /// <value>Layers</value>
        public List<IStageLayer> StageLayers { get; private set; } = new List<IStageLayer>();

        /// <summary>
        /// The sound controller used for this stage controller.
        /// </summary>
        /// <value>Sound Controller</value>
        public SoundController SoundController { get; set; }

        #endregion

        #region Controller Methods

        /// <summary>
        /// Initializes the controller.
        /// </summary>
        public void Initialize()
        {
            
        }

        /// <summary>
        /// Updates the state of the controller.
        /// </summary>
        public void Update()
        {
            for(int i = 0; i < StageLayers.Count; i++)
            {
                StageLayers[i].DoUpdate();
            }
        }

        /// <summary>
        /// Clears the stage data.
        /// </summary>
        public void Clear()
        {
            for(int i =0; i < StageLayers.Count; i++)
            {
                if (!StageLayers[i].Persistent)
                {
                    StageLayers[i].ClearAll();
                }
            }
        }

        /// <summary>
        /// Completely clears all stage data.
        /// </summary>
        public void ClearAll()
        {
            for (int i = 0; i < StageLayers.Count; i++)
            {
                StageLayers[i].ClearAll();
            }
        }

        #endregion

        #region Layer Management

        /// <summary>
        /// Returns a layer by the LayerID.
        /// </summary>
        /// <param name="LayerID">Layer ID.</param>
        /// <returns>The layer</returns>
        public IStageLayer GetLayer(string LayerID)
        {
            return StageLayers.Find(x => x.ID == LayerID);
        }
        
        /// <summary>
        /// Adds a layer to the list.
        /// </summary>
        /// <param name="Layer">Layer to add.</param>
        public void AddLayer(IStageLayer Layer)
        {
            StageLayers.Add(Layer);
            Layer.DoInitialize(null);
        }

        /// <summary>
        /// Inserts a layer into the list.
        /// </summary>
        /// <param name="Layer">Layer to add.</param>
        /// <param name="Index">Index</param>
        public void InsertLayer(int Index, IStageLayer Layer)
        {
            StageLayers.Insert(Index,Layer);
        }

        /// <summary>
        /// Swaps the position of two layers.
        /// </summary>
        /// <param name="Index1">First layer to swap</param>
        /// <param name="Index2">Second layer to swap</param>
        public void SwapLayer(int Index1, int Index2)
        {
            IStageLayer[] layers = new IStageLayer[2];

            layers[0] = StageLayers[Index1];
            layers[1] = StageLayers[Index2];

            StageLayers[Index2] = layers[0];
            StageLayers[Index1] = layers[1];
        }

        /// <summary>
        /// Removes a layer from the list.
        /// </summary>
        /// <param name="Layer">Layer</param>
        public void RemoveLayer(IStageLayer Layer)
        {
            StageLayers.Remove(Layer);
        }

        /// <summary>
        /// Returns if the stage controller contains this layer.
        /// </summary>
        /// <param name="Layer">Layer to check</param>
        /// <returns>True/False</returns>
        public bool ContainsLayer(IStageLayer Layer)
        {
            return StageLayers.Contains(Layer);
        }
        #endregion

        #region Serialize

        /// <summary>
        /// Serializes the layers in the controller into a MaxSerializedObject.
        /// </summary>
        /// <returns>Serialized object</returns>
        public MaxSerializedObject Serialize()
        {
            MaxSerializedObject Data = MaxSerializedObject.Create();
            Data.AddSerializableObjectCollection(SER_LAYERS, StageLayers.FindAll(x => x.Serializable).ToArray());
            Data.SetType(GetType());
            return Data;
        }

        /// <summary>
        /// Deserializes a MaxSerializedObject into layers.
        /// </summary>
        /// <param name="Data">Data</param>
        public void Deserialize(MaxSerializedObject Data)
        {
            ClearAll();
            List<MaxSerializedObject> objects = Data.GetSerializedObjectCollection(SER_LAYERS);
            IStageLayer layer;
            for(int i = 0; i < objects.Count; i++)
            {
               layer = (IStageLayer)objects[i].Deserialize();
               layer.StageController = this;
               StageLayers.Add(layer);
            }
        }

        /// <summary>
        /// Creates an instance of this type of object from the provided data.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Deserialized new instance</returns>
        public IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable a = new StageController();
            a.Deserialize(Data);
            return a;
        }

        #endregion

    }
}

﻿using System.Collections.Generic;
using max.stage.component;
using System;
using Microsoft.Xna.Framework;
using max.geometry.shape2D;
using max.stage.layer.template;
using max.serialize;

namespace max.stage.layer
{

    /// <summary>
    /// This layer represents collision objects, or objects that use collisions to interact.
    /// For example, a collision layer can be used for walls, but also events that trigger
    /// when collided with.
    /// </summary>
    public class CollisionLayer : ComponentLayer
    {
        #region Properties

        /// <summary>
        /// The items in the layer.
        /// </summary>
        /// <value>Items</value>
        public override List<ILayerComponent> Components { get; protected set; } = new List<ILayerComponent>();

        /// <summary>
        /// The polygons of all the collisions.
        /// </summary>
        /// <value>Polygons</value>
        public List<Polygon2D> Polygons
        {
            get
            {
                List<Polygon2D> polygons = new List<Polygon2D>();
                for (int i = 0; i < Collisions.Count; i++)
                {
                    polygons.Add(Collisions[i].Border);
                }

                return polygons;
            }
        }

        /// <summary>
        /// The list of collision components.
        /// </summary>
        /// <value>Collision list</value>
        public List<CollisionComponent> Collisions
        {
            get
            {
                return _collisions;
            }
            set
            {
                _collisions = value;
                Components = new List<ILayerComponent>(_collisions);
            }
        }
            
        List<CollisionComponent> _collisions = new List<CollisionComponent>();

        #endregion

        #region Constructors
        /// <summary>
        /// Creates an instance with an ID and a reference to a parent StageController.
        /// </summary>
        /// <param name="ID">Layer ID</param>
        /// <param name="StageController">Stage Controller</param>

        public CollisionLayer(string ID, StageController StageController) : base(ID, StageController)
        {
        }

        /// <summary>
        /// Creates an empty collision layer instance.
        /// </summary>
        public CollisionLayer() : base() {}

        #endregion

        #region Overriden methods
        /// <summary>
        /// Adds an item to the layer.
        /// </summary>
        /// <param name="Item">The item to add.</param>
        public override void AddComponent(ILayerComponent Item)
        {
            Item.DoInitialize(this);
            Collisions.Add((CollisionComponent)Item);
        }

        /// <summary>
        /// Adds a collision based on a polygon to the layer.
        /// </summary>
        /// <param name="Polygon">Polygon</param>
        /// <returns>Collision component</returns>
        public CollisionComponent AddComponent(Polygon2D Polygon)
        {
            CollisionComponent comp = new CollisionComponent(Polygon);
            comp.DoInitialize(this);
            Collisions.Add(comp);
            return comp;
        }

        /// <summary>
        /// Adds collisions based on an array of polygons to the layer.
        /// </summary>
        /// <param name="Polygons">Polygons</param>
        /// <returns>Collision components</returns>
        public CollisionComponent[] AddComponents(Polygon2D[] Polygons)
        {
            CollisionComponent[] comps = new CollisionComponent[Polygons.Length];
            for (int i = 0; i < Polygons.Length; i++)
            {
                comps[i] = new CollisionComponent(Polygons[i]);
                comps[i].DoInitialize(this);
                Collisions.Add(comps[i]);
            }
            return comps;
        }

        /// <summary>
        /// Adds an item to the layer.
        /// </summary>
        /// <param name="Item">The item to add.</param>
        public override void RemoveComponent(ILayerComponent Item)
        {
            Item.DoDispose();
            Collisions.Remove((CollisionComponent)Item);
        }

        /// <summary>
        /// Clears the layer's contents.
        /// </summary>
        public override void Clear()
        {
            for (int i = 0; i < Collisions.Count; i++)
            {
                Collisions[i].DoDispose();
            }
            Collisions.Clear();
        }
        #endregion

        #region Class specific methods

        /// <summary>
        /// Gets an array of collisions that collide with a point.
        /// </summary>
        /// <param name="Point">The point to test.</param>
        /// <returns>The array of all collisions with the point</returns>
        public List<CollisionComponent> GetCollisions(Vector2 Point)
        {
            List<CollisionComponent> collisions = new List<CollisionComponent>();
            for(int i = 0; i < Collisions.Count; i++)
            {
                if (Collisions[i].Border.PointInGeometry(Point))
                {
                    collisions.Add(Collisions[i]);
                }
            }

            return collisions;
        }

        /// <summary>
        /// Gets an array of collisions that collide with geometry.
        /// </summary>
        /// <param name="Geometry">The geometry to test.</param>
        /// <returns>The array of all collisions with the point</returns>
        public List<CollisionComponent> GetCollisions(I2DGeometry Geometry)
        {
            List<CollisionComponent> collisions = new List<CollisionComponent>();
            for (int i = 0; i < Collisions.Count; i++)
            {
                if (Collisions[i].Border.IntersectPoints(Geometry).Count > 0)
                {
                    collisions.Add(Collisions[i]);
                }
            }

            return collisions;
        }

        /// <summary>
        /// Check whether a point is colliding with anything on this layer.
        /// </summary>
        /// <param name="Point">The point to test.</param>
        /// <returns>True/False</returns>
        public bool IsColliding(Vector2 Point)
        {
            return GetCollisions(Point).Count > 0;
        }

        /// <summary>
        /// Check whether a geometry is colliding with anything on this layer.
        /// </summary>
        /// <param name="Geometry">The geometry to test.</param>
        /// <returns>True/False</returns>
        public bool IsColliding(I2DGeometry Geometry)
        {
            return GetCollisions(Geometry).Count > 0;
        }

        #endregion

        #region Serialize

        /// <summary>
        /// Deserialize the data into the layer.
        /// </summary>
        /// <param name="Data">Data</param>
        public override void Deserialize(MaxSerializedObject Data)
        {
            Clear();
            List<MaxSerializedObject> obj = Data.GetSerializedObjectCollection(SER_DATA);
            for(int i = 0; i < obj.Count; i++)
            {
                Collisions.Add((CollisionComponent)obj[i].Deserialize());
            }
        }

        /// <summary>
        /// Creates an instance of this type of object from the provided data.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Deserialized new instance</returns>
        public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable a = new CollisionLayer();
            a.Deserialize(Data);
            return a;
        }
        #endregion
    }
}

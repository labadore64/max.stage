﻿using max.serialize;
using max.stage;
using max.stage.component;
using max.stage.layer.template;

namespace stage.layer.controller
{
    /// <summary>
    /// Represents a controller used in ControllerLayer.
    /// </summary>
    public abstract class Controller : ILayerComponent
    {
        #region Properties
        /// <summary>
        /// Whether or not the controller has been initialized.
        /// </summary>
        /// <value>True/False</value>
        public virtual bool Initialized { get; protected set; }
        /// <summary>
        /// Whether or not the controller is active.
        /// </summary>
        /// <value>True/False</value>
        public virtual bool Active { get; set; } = true;
        /// <summary>
        /// Whether or not the controller has been disposed of.
        /// </summary>
        /// <value>True/False</value>
        public virtual bool Disposed { get; protected set; }
        /// <summary>
        /// Whether or not the controller is persistent
        /// </summary>
        /// <value>True/False</value>
        public virtual bool Persistent { get; set; } = true;
        /// <summary>
        /// Whether or not the controller is serializable.
        /// </summary>
        /// <value>True/False</value>
        public virtual bool Serializable { get; set; }
        /// <summary>
        /// The parent layer.
        /// </summary>
        /// <value>Parent</value>
        public virtual IStageLayer Parent { get; set; }
        /// <summary>
        /// The stage controller.
        /// </summary>
        /// <value>Stage controller</value>
        public virtual StageController StageController
        {
            get { if (Parent != null) { return Parent.StageController; } return _stageController; }
            set { _stageController = value; }
        }

        StageController _stageController;
        #endregion

        #region ControllerMethods

        /// <summary>
        /// Starts initializing the layer.
        /// </summary>
        /// <param name="StageLayer">Parent</param>
        public void DoInitialize(IStageLayer StageLayer)
        {
            if (!Initialized)
            {
                Parent = StageLayer;
                Initialize();
                Initialized = true;
            }
        }

        /// <summary>
        /// Begins updating the layer.
        /// </summary>
        public void DoUpdate()
        {
            if (Initialized && !Disposed && Active)
            {
                Update();
            }
        }

        /// <summary>
        /// Begins disposing of the layer.
        /// </summary>
        public void DoDispose()
        {
            if (!Disposed && Initialized)
            {
                Dispose();
                Disposed = true;
            }
        }

        /// <summary>
        /// Initializes the controller.
        /// </summary>
        public virtual void Initialize()
        {
            // do nothing
        }
        /// <summary>
        /// Updates the state of the controller.
        /// </summary>
        public virtual void Update()
        {
            // do nothing
        }
        /// <summary>
        /// Disposes of the controller's resources.
        /// </summary>
        public virtual void Dispose()
        {
            // do nothing
        }

        #endregion

        #region Serialize
        /// <summary>
        /// Serializes the controller into a MaxSerializedObject.
        /// </summary>
        /// <returns>Data</returns>
        public abstract MaxSerializedObject Serialize();
        /// <summary>
        /// Deserializes the data into the controller.
        /// </summary>
        /// <param name="Data">Data</param>
        public abstract void Deserialize(MaxSerializedObject Data);
        /// <summary>
        /// Creates a new instance of this controller with the Data provided.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Instance</returns>
        public abstract IMaxSerializable CreateInstance(MaxSerializedObject Data);
        #endregion
    }
}

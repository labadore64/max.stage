﻿using max.serialize;
using max.sound.controller;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace stage.layer.controller
{
    /// <summary>
    /// A controller that represents a player's position moving around on a stage.
    /// </summary>
    public class Player2DController : Controller
    {

        #region Constants
        const string SER_POS = "pos";
        const string SER_LOOKPOS = "lookpos";
        #endregion

        #region Properties
        /// <summary>
        /// This represents the position of the player. 
        /// </summary>
        /// <value>Position</value>
        public Vector2 Position 
        {
            get { return SoundController.Position; }
            set 
            { 
                SoundController.Position = value;
                UpdateTriggerVector();
            }
        }

        // used to stage the position when initializing the layer
        Vector2 stagePosition = Vector2.Zero;

        /// <summary>
        /// This represents the point that the player is "looking" at,
        /// relative to the map.
        /// </summary>
        /// <value>Look Vector</value>
        public Vector2 LookVector
        {
            get { return SoundController.LookVector; }
            set 
            { 
                SoundController.LookVector = value;
                UpdateTriggerVector();
            }
        }

        // used to stage the look vector when initializing the layer
        Vector2 stageLookVector = Vector2.Zero;

        /// <summary>
        /// This represents the angle that the player is "looking".
        /// </summary>
        /// <value>Angle</value>
        public float LookAngle
        {
            get { return SoundController.LookAngle; }
            set 
            { 
                SoundController.LookAngle = value;
                UpdateTriggerVector();
            }
        }

        /// <summary>
        /// This represents the distance the player is "looking".
        /// </summary>
        /// <value>Distance</value>
        public float LookDistance
        {
            get { return SoundController.LookDistance; }
            set 
            { 
                SoundController.LookDistance = value;
                UpdateTriggerVector();
            }
        }

        /// <summary>
        /// Represents the sound controller used by the player.
        /// </summary>
        /// <value>Sound Controller</value>
        public SoundController SoundController
        {
            get { return StageController.SoundController; }
        }

        /// <summary>
        /// Represents a list of properties indicating the current state of the player.
        /// </summary>
        /// <value>Property dictionary</value>
        public Dictionary<string, object> Properties { get; private set; } = new Dictionary<string, object>();

        #region Trigger properties

        /// <summary>
        /// The distance in front of the player's position that
        /// activates triggered functions, such as interacting
        /// with objects. This makes it so that you have to face
        /// an object in order to interact with it. The further this distance,
        /// the further out you can interact with objects.
        /// </summary>
        public float TriggerDistance
        {
            get { return _triggerDistance; }

            set
            {
                _triggerDistance = value;
                UpdateTriggerVector();
            }
        }

        float _triggerDistance = 15;

        /// <summary>
        /// This is the point used to test triggers on a map.
        /// It faces the same direction as the Look Vector but has its own radius,
        /// allowing interactivity to only occur if the interactable object
        /// is facing the right direction.
        /// </summary>
        /// <value>Point</value>
        public Vector2 TriggerVector
        {
            get
            {
                return _triggerVector;
            }
        }

        Vector2 _triggerVector;

        #endregion

        #endregion

        #region Controller

        /// <summary>
        /// Initializes the location of the Position and LookVector, if not already set.
        /// </summary>
        public override void Initialize()
        {
            if (SoundController != null)
            {
                if (Position != Vector2.Zero)
                {
                    Position = stagePosition;
                }
                if (LookVector != Vector2.Zero)
                {
                    LookVector = stageLookVector;
                }
            }
        }

        #endregion

        #region Serialize

        /// <summary>
        /// Serializes the Player2DController into a MaxSerializedObject.
        /// </summary>
        /// <returns>Data</returns>
        public override MaxSerializedObject Serialize()
        {
            MaxSerializedObject Data = MaxSerializedObject.Create();

            Data.AddVector2(SER_POS, Position);
            Data.AddVector2(SER_LOOKPOS, LookVector);

            return Data;

        }

        /// <summary>
        /// Deserializes the data into a Player2DController.
        /// </summary>
        /// <param name="Data">Data</param>
        public override void Deserialize(MaxSerializedObject Data)
        {
            stagePosition = Data.GetVector2(SER_POS);
            stageLookVector = Data.GetVector2(SER_LOOKPOS);
        }

        /// <summary>
        /// Creates an instance of Player2DController.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Instance</returns>
        public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable obj = new Player2DController();
            obj.Deserialize(Data);
            return obj;
        }
        #endregion

        #region Internal
        private void UpdateTriggerVector()
        {
            float ang = LookAngle;
            float x = Position.X + (_triggerDistance * (float)Math.Cos(ang));
            float y = Position.Y - (_triggerDistance * (float)Math.Sin(ang));
            _triggerVector = new Vector2(x, y);
        }
        #endregion
    }
}

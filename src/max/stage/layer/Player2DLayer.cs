﻿using max.serialize;
using max.stage.component;
using max.stage.layer.template;
using Microsoft.Xna.Framework;
using stage.layer.controller;

namespace max.stage.layer
{
    /// <summary>
    /// Represents a layer containing a Player2DController.
    /// </summary>
    public class Player2DLayer : ControllerLayer
    {
        #region Properties

        /// <summary>
        /// The player controller.
        /// </summary>
        /// <value>Player controller</value>
        public Player2DController PlayerController { get; protected set; } = new Player2DController();

        /// <summary>
        /// The controller.
        /// </summary>
        /// <value>Controller</value>
        public override ILayerComponent Controller 
        {
            get { return PlayerController; }
            protected set
            {
                PlayerController = (Player2DController)value;
            }
        }

        /// <summary>
        /// Position of the player.
        /// </summary>
        /// <value>Position</value>
        public Vector2 Position
        {
            get
            {
                return PlayerController.Position;
            }
            set
            {
                PlayerController.Position = value;
            }
        }

        /// <summary>
        /// Position of the look vector of the player.
        /// </summary>
        /// <value>Look Vector</value>
        public Vector2 LookVector
        {
            get
            {
                return PlayerController.LookVector;
            }
            set
            {
                PlayerController.LookVector = value;
            }
        }

        /// <summary>
        /// Angle of the look vector of the player.
        /// </summary>
        /// <value>Angle</value>
        public float LookAngle
        {
            get
            {
                return PlayerController.LookAngle;
            }
            set
            {
                PlayerController.LookAngle = value;
            }
        }

        /// <summary>
        /// Distance of the look vector of the player.
        /// </summary>
        /// <value>Distance</value>
        public float LookDistance
        {
            get
            {
                return PlayerController.LookDistance;
            }
            set
            {
                PlayerController.LookDistance = value;
            }
        }

        public float TriggerDistance
        {
            get { return PlayerController.TriggerDistance; }

            set
            {
                PlayerController.TriggerDistance = value;
            }
        }

        /// <summary>
        /// This is the point used to test triggers on a map.
        /// It faces the same direction as the Look Vector but has its own radius,
        /// allowing interactivity to only occur if the interactable object
        /// is facing the right direction.
        /// </summary>
        /// <value>Point</value>
        public Vector2 TriggerVector
        {
            get
            {
                return PlayerController.TriggerVector;
            }
        }

        #endregion

        /// <summary>
        /// Default constructor. 
        /// </summary>
        public Player2DLayer() : base()
        {

        }

        /// <summary>
        /// Using ID and Stage Controller.
        /// </summary>
        /// <param name="ID">ID</param>
        /// <param name="StageController">Stage Controller</param>
        public Player2DLayer(string ID, StageController StageController) : base(ID, StageController)
        {
            this.StageController = StageController;
            this.ID = ID;
        }

        /// <summary>
        /// Creates an instance of PlayerLayer from the serialized data.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>PlayerLayer instance</returns>
        public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable obj = new Player2DLayer();
            obj.Deserialize(Data);
            return obj;
        }
    }
}

﻿using max.serialize;
using max.stage.component;
using System.Collections.Generic;

namespace max.stage.layer.template
{
    /// <summary>
    /// This interface represents a layer to be added to the Stage Controller.
    /// </summary>
    public interface IStageLayer : ILayerComponent, IMaxSerializable
    {
        #region Properties

        /// <summary>
        /// The components in this layer.
        /// </summary>
        /// <value>Items</value>
        List<ILayerComponent> Components { get; }

        /// <summary>
        /// The ID of this layer, used for reference purposes.
        /// </summary>
        /// <value>ID</value>
        string ID { get; }

        #endregion

        #region Layer Control

        /// <summary>
        /// Adds a new component to the layer.
        /// </summary>
        /// <param name="Item">Component to add</param>
        void AddComponent(ILayerComponent Item);
        
        /// <summary>
        /// Removes a component from the layer.
        /// </summary>
        /// <param name="Item">Component to remove</param>
        void RemoveComponent(ILayerComponent Item);

        /// <summary>
        /// Clears everything off this layer.
        /// </summary>
        void Clear();

        /// <summary>
        /// Clears everything off this layer, including persistent components.
        /// </summary>
        void ClearAll();

        #endregion
    }
}

﻿using max.serialize;
using max.stage.component;
using System.Collections.Generic;

namespace max.stage.layer.template
{
    /// <summary>
    /// An abstract class that represents a controller. By default, controller layers are persistent and must be manually removed.
    /// </summary>
    public abstract class ControllerLayer : Layer
    {

        #region Properties

        /// <summary>
        /// Whether or not this component is serialized when the parent's Serialize method is called.
        /// </summary>
        /// <value>True/False</value>
        public override bool Serializable { get; set; } = false;

        /// <summary>
        /// Returns a list with the controller as its only item.
        /// </summary>
        /// <value>Items</value>
        public override List<ILayerComponent> Components 
        { 
            get 
            { 
                return new List<ILayerComponent>(
                    new ILayerComponent[] { Controller }
                    ); 
            }
            protected set { if (value.Count > 0) { Controller = value[0]; } }
        }

        /// <summary>
        /// The controller that the layer is managing.
        /// </summary>
        /// <value>Controller</value>
        public virtual ILayerComponent Controller { get; protected set; }

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a controller layer with an ID and parent Stage Controller.
        /// </summary>
        /// <param name="ID">ID</param>
        /// <param name="StageController">Stage Controller</param>
        public ControllerLayer(string ID, StageController StageController) : base(ID, StageController)
        {
            Persistent = true;
        }

        /// <summary>
        /// Creates an empty controller layer.
        /// </summary>
        public ControllerLayer()
        {
            Persistent = true;
        }
        #endregion

        #region Control Methods

        /// <summary>
        /// Dispose this layer.
        /// </summary>
        public override void Dispose()
        {
            Disposed = true;
            if (Controller != null)
            {
                Controller.DoDispose();
            }
        }

        /// <summary>
        /// Initialize the layer.
        /// </summary>
        /// <param name="StageLayer">Parent</param>
        public override void Initialize()
        {
            if(Controller != null)
            {
                Controller.DoInitialize(this);
            }
        }

        /// <summary>
        /// Updates the state of the layer.
        /// </summary>
        public override void Update()
        {
            if (StageController != null)
            {
                if (Controller != null)
                {
                    Controller.DoUpdate();
                }
            }
        }

        /// <summary>
        /// Does nothing, inherited from IStageLayer
        /// </summary>
        public override void Clear()
        {
            // Does nothing
        }

        /// <summary>
        /// Does nothing, inherited from IStageLayer
        /// </summary>
        public override void ClearAll()
        {
            // does nothing
        }

        #endregion

        #region Layer Control

        /// <summary>
        /// Sets the controller.
        /// </summary>
        /// <param name="Item">Component to add</param>
        public override void AddComponent(ILayerComponent Item)
        {
            Controller = Item;
        }

        /// <summary>
        /// Sets the controller to null.
        /// </summary>
        /// <param name="Item">Component to remove</param>
        public override void RemoveComponent(ILayerComponent Item)
        {
            Controller = null;
        }

        #endregion

        #region serialize

        /// <summary>
        /// Serialize the layer into a dictionary.
        /// </summary>
        /// <returns>Serialized data</returns>
        protected override MaxSerializedObject DoSerialize(MaxSerializedObject Data)
        {
            Data.AddSerializedObject(SER_DATA, Controller);

            return Data;
        }

        /// <summary>
        /// Deserialize the data into the layer.
        /// </summary>
        /// <param name="Data">Data</param>
        public override void Deserialize(MaxSerializedObject Data)
        {
            Controller = (ILayerComponent)Data.GetSerializedObject(SER_DATA).Deserialize();
        }
        #endregion
    }
}

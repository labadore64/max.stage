﻿using max.serialize;
using max.stage.component;
using System.Collections.Generic;

namespace max.stage.layer.template
{
    /// <summary>
    /// An abstract class that represents a set of components.
    /// </summary>
    public abstract class ComponentLayer : Layer
    {
        #region Properties

        /// <summary>
        /// The items in the current layer.
        /// You can override this in child classes.
        /// </summary>
        /// <value>Items</value>
        public override List<ILayerComponent> Components { get; protected set; } = new List<ILayerComponent>();

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a component layer with an ID and parent Stage Controller.
        /// </summary>
        /// <param name="ID">ID</param>
        /// <param name="StageController">Stage Controller</param>
        public ComponentLayer(string ID, StageController StageController) : base(ID, StageController)
        {
        }

        /// <summary>
        /// Creates an empty component layer.
        /// </summary>
        protected ComponentLayer()
        {
        }
        #endregion

        #region Control Methods

        /// <summary>
        /// Clears the layer's contents.
        /// </summary>
        public override void Clear()
        {
            for(int i = 0; i < Components.Count; i++)
            {
                if (!Components[i].Persistent)
                {
                    Components[i].DoDispose();
                    RemoveComponent(Components[i]);
                }
            }
        }

        /// <summary>
        /// Clears the layer's contents, including persistent objects.
        /// </summary>
        public override void ClearAll()
        {
            for (int i = 0; i < Components.Count; i++)
            {
                Components[i].DoDispose();
                RemoveComponent(Components[i]);
            }
        }

        #endregion

        #region Layer Control

        /// <summary>
        /// Adds a new component to the layer.
        /// </summary>
        /// <param name="Item">Component to add</param>
        public override void AddComponent(ILayerComponent Item)
        {
            Item.DoInitialize(this);
            Components.Add(Item);
        }

        /// <summary>
        /// Removes a component from the layer.
        /// </summary>
        /// <param name="Item">Component to remove</param>
        public override void RemoveComponent(ILayerComponent Item)
        {
            Item.DoDispose();
            Components.Remove(Item);
        }

        #endregion

        #region serialize

        /// <summary>
        /// Serialize the layer into a dictionary.
        /// </summary>
        /// <returns>Serialized data</returns>
        protected override MaxSerializedObject DoSerialize(MaxSerializedObject Data)
        {
            Data.SetType(GetType());
            Data.AddSerializableObjectCollection(SER_DATA, Components.FindAll(x => x.Serializable).ToArray());

            return Data;
        }

        /// <summary>
        /// Deserialize the data into the layer.
        /// </summary>
        /// <param name="Data">Data</param>
        public override void Deserialize(MaxSerializedObject Data)
        {
            Clear();
            List<MaxSerializedObject> objs = Data.GetSerializedObjectCollection(SER_DATA);
            ILayerComponent comp;
            for(int i = 0; i < objs.Count; i++)
            {
                comp = (ILayerComponent)objs[i].Deserialize();
                comp.DoInitialize(this);
                Components.Add(comp);
            }
        }
        #endregion
    }
}

﻿using System.Collections.Generic;
using max.serialize;
using max.stage.component;

namespace max.stage.layer.template
{
    /// <summary>
    /// An abstract class that represents the default layer structure.
    /// </summary>
    public abstract class Layer : IStageLayer
    {
        /// <summary>
        /// Whether or not this layer has been initialized.
        /// </summary>
        /// <value>True/False</value>
        public virtual bool Initialized { get; protected set; }

        /// <summary>
        /// Whether or not this component is serialized when the parent's Serialize method is called.
        /// </summary>
        /// <value>True/False</value>
        public virtual bool Serializable { get; set; } = true;

        /// <summary>
        /// Serialization ID for layer data.
        /// </summary>
        /// <value>"data"</value>
        protected const string SER_DATA = "data";

        /// <summary>
        /// Serialization ID for the layer ID.
        /// </summary>
        /// <value>"layer_id"</value>
        protected const string SER_ID = "layer_id";

        /// <summary>
        /// Whether or not the layer is removed when the room is cleared. If true,
        /// you must manually remove the layer yourself.
        /// </summary>
        /// <value>True/False</value>
        public virtual bool Persistent { get; set; }

        /// <summary>
        /// The parent stage controller.
        /// </summary>
        /// <value>Stage controller</value>
        public StageController StageController { get; set; }

        /// <summary>
        /// The parent layer.
        /// </summary>
        /// <value>Layer</value>
        public IStageLayer Parent { get; set; }
        /// <summary>
        /// Whether or not this layer is active.
        /// </summary>
        /// <value>True/False</value>
        public virtual bool Active { get; set; } = true;

        /// <summary>
        /// The ID of this layer, used for reference purposes.
        /// </summary>
        /// <value>ID</value>
        public virtual string ID { get; protected set; }

        /// <summary>
        /// Whether or not the layer has been disposed of.
        /// </summary>
        /// <value>ID</value>
        public virtual bool Disposed { get; protected set; }

        /// <summary>
        /// The list of components in the layer.
        /// </summary>
        /// <value>Components</value>
        public abstract List<ILayerComponent> Components { get; protected set; }

        #region Constructors

        /// <summary>
        /// Create an empty layer.
        /// </summary>
        public Layer()
        {

        }

        /// <summary>
        /// Create a layer with an ID and reference to a parent StageController.
        /// </summary>
        /// <param name="ID">ID</param>
        /// <param name="StageController">Stage Controller</param>
        public Layer(string ID, StageController StageController)
        {
            this.ID = ID;
            this.StageController = StageController;
        }

        #endregion

        #region Controller Methods
        /// <summary>
        /// Starts initializing the layer.
        /// </summary>
        /// <param name="StageLayer">Parent</param>
        public void DoInitialize(IStageLayer StageLayer)
        {
            if (!Initialized)
            {
                Parent = StageLayer;
                Initialize();
                Initialized = true;
            }
        }

        /// <summary>
        /// Initializes the layer.
        /// </summary>
        public virtual void Initialize()
        {
            List<ILayerComponent> Item = Components;
            for (int i = 0; i < Item.Count; i++)
            {
                Item[i].DoInitialize(this);
            }
        }

        /// <summary>
        /// Begins disposing of the layer.
        /// </summary>
        public void DoDispose()
        {
            if(!Disposed && Initialized)
            {
                ClearAll();
                Dispose();
                Disposed = true;
            }
        }

        /// <summary>
        /// Dispose this layer.
        /// </summary>
        public virtual void Dispose()
        {
            List<ILayerComponent> Item = Components;
            for (int i = 0; i < Item.Count; i++)
            {
                Item[i].DoDispose();
            }
        }

        /// <summary>
        /// Begins updating the layer.
        /// </summary>
        public void DoUpdate()
        {
            if(Initialized && !Disposed && Active)
            {
                Update();
            }
        }

        /// <summary>
        /// Updates the state of the layer.
        /// </summary>
        public virtual void Update()
        {
            if (StageController != null)
            {
                for (int i = 0; i < Components.Count; i++)
                {
                    Components[i].DoUpdate();
                }
            }
        }

        /// <summary>
        /// Adds a component to the list.
        /// </summary>
        /// <param name="Item">Component</param>
        public abstract void AddComponent(ILayerComponent Item);

        /// <summary>
        /// Removes a component from the list.
        /// </summary>
        /// <param name="Item">Component</param>
        public abstract void RemoveComponent(ILayerComponent Item);
        /// <summary>
        /// Clears all non-persistent components from the list and properly
        /// disposes of them.
        /// </summary>
        public abstract void Clear();
        /// <summary>
        /// Clears all components from the list and properly disposes of them.
        /// </summary>
        public abstract void ClearAll();

        #endregion

        #region Serialize

        /// <summary>
        /// Serializes the layer into a MaxSerializedObject.
        /// </summary>
        /// <returns>Data</returns>
        public MaxSerializedObject Serialize()
        {
            MaxSerializedObject Data = MaxSerializedObject.Create();
            if (Serializable)
            {
                Data = DoSerialize(Data);
            }
            Data.SetType(GetType());
            Data.AddString(SER_ID, ID);
            return Data;
        }

        /// <summary>
        /// Deserializes the MaxSerializedObject into the layer.
        /// </summary>
        /// <param name="Data">Data</param>
        public virtual void Deserialize(MaxSerializedObject Data) 
        {
            ID = Data.GetString(SER_ID);
        }

        /// <summary>
        /// Serialize the layer into a dictionary.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Serialized data</returns>
        protected virtual MaxSerializedObject DoSerialize(MaxSerializedObject Data)
        {
            Data.AddSerializableObjectCollection(SER_DATA, Components.ToArray());

            return Data;
        }

        /// <summary>
        /// Creates an instance of this type of object from the provided data.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Deserialized new instance</returns>
        public abstract IMaxSerializable CreateInstance(MaxSerializedObject Data);
        #endregion
    }
}

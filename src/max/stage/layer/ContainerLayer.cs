﻿using max.serialize;
using max.stage.component;
using max.stage.layer.template;
using max.util;
using System;
using System.Collections.Generic;

namespace max.stage.layer
{
    /// <summary>
    /// An abstract class that represents a layer containing layers.
    /// </summary>
    public class ContainerLayer : Layer
    {

        #region Properties

        /// <summary>
        /// The items in the layer.
        /// </summary>
        /// <value>Items</value>
        public override List<ILayerComponent> Components
        {
            get
            {
                return new List<ILayerComponent>(Layers);
            }
            protected set
            {
                throw new NotImplementedException("This is intentionally unimplemented!");
            }
        }

        /// <summary>
        /// The list of layers
        /// </summary>
        /// <value>Layer list</value>
        public List<IStageLayer> Layers { get; private set; } = new List<IStageLayer>();

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a container layer with an ID and a reference to a parent Stage Controller.
        /// </summary>
        /// <param name="ID">ID</param>
        /// <param name="StageController">Stage Controller</param>
        public ContainerLayer(string ID, StageController StageController) : base(ID, StageController)
        {
        }

        /// <summary>
        /// Creates an empty container layer.
        /// </summary>
        public ContainerLayer() : base(){}

        #endregion

        #region Controller methods

        /// <summary>
        /// Clears all layers from the list except those marked as
        /// persistent and properly disposes of them.
        /// </summary>
        public override void Clear()
        {
            for (int i = 0; i < Layers.Count; i++)
            {
                if (!Layers[i].Persistent)
                {
                    Layers[i].DoDispose();
                    RemoveComponent(Layers[i]);
                    i--;
                }
            }
        }

        /// <summary>
        /// Clears all layers from the list and properly disposes of them,
        /// ignoring their persistence state.
        /// </summary>
        public override void ClearAll()
        {
            for (int i = 0; i < Layers.Count; i++)
            {
                Layers[i].DoDispose();
                RemoveComponent(Layers[i]);
                i--;
            }
        }

        #endregion

        #region Add/Remove etc.

        /// <summary>
        /// Adds an item to the layer.
        /// </summary>
        /// <param name="Item">The item to add.</param>
        public override void AddComponent(ILayerComponent Item)
        {
            if (!(Item is IStageLayer))
            {
                throw new ArgumentException(
                    ReflectionUtil.FullTypeToString(Item.GetType())
                    + " must implement type " +
                    ReflectionUtil.FullTypeToString(typeof(ILayerComponent)));
            }

            Item.DoInitialize(this);
            Layers.Add((IStageLayer)Item);
        }

        /// <summary>
        /// Adds an item to the layer.
        /// </summary>
        /// <param name="Item">The item to add.</param>
        public override void RemoveComponent(ILayerComponent Item)
        {
            if(!(Item is IStageLayer))
            {
                throw new ArgumentException(
                    ReflectionUtil.FullTypeToString(Item.GetType()) 
                    + " must implement type " + 
                    ReflectionUtil.FullTypeToString(typeof(ILayerComponent)));
            }

            Item.DoDispose();
            Layers.Remove((IStageLayer)Item);
        }

        /// <summary>
        /// Adds a layer to the list.
        /// </summary>
        /// <param name="Layer">Layer</param>
        public void AddLayer(IStageLayer Layer)
        {
            Layer.DoInitialize(this);
            AddComponent(Layer);
        }

        /// <summary>
        /// Removes a layer from the list.
        /// </summary>
        /// <param name="Layer">Layer</param>
        public void RemoveLayer(IStageLayer Layer)
        {
            RemoveComponent(Layer);
        }

        /// <summary>
        /// Returns a layer by the LayerID.
        /// </summary>
        /// <param name="LayerID">Layer ID.</param>
        /// <returns>The layer</returns>
        public IStageLayer GetLayer(string LayerID)
        {
            return Layers.Find(x => x.ID == LayerID);
        }

        /// <summary>
        /// Returns if the stage controller contains this layer.
        /// </summary>
        /// <param name="Layer">Layer to check</param>
        /// <returns>True/False</returns>
        public bool ContainsLayer(IStageLayer Layer)
        {
            return Layers.Contains(Layer);
        }

        #endregion

        #region Serialize

        /// <summary>
        /// Serialize the layer into a dictionary.
        /// </summary>
        /// <returns>Serialized data</returns>
        protected override MaxSerializedObject DoSerialize(MaxSerializedObject Data)
        {
            Data.AddSerializableObjectCollection(SER_DATA, Layers.FindAll(x => x.Serializable).ToArray());

            return Data;
        }

        /// <summary>
        /// Deserialize the data into the layer.
        /// </summary>
        /// <param name="Data">Data</param>
        public override void Deserialize(MaxSerializedObject Data)
        {
            Clear();
            base.Deserialize(Data);
            List<MaxSerializedObject> list = Data.GetSerializedObjectCollection(SER_DATA);
            IStageLayer layer;
            for (int i = 0; i < list.Count; i++)
            {
                layer = (IStageLayer)list[i].Deserialize();
                layer.StageController = StageController;
                layer.DoInitialize(this);
                Layers.Add(layer);
            }
        }

        /// <summary>
        /// Creates an instance of this type of object from the provided data.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Deserialized new instance</returns>
        public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable a = new ContainerLayer();
            a.Deserialize(Data);
            return a;
        }

        #endregion
    }
}

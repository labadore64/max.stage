﻿using max.geometry.shape2D;
using max.serialize;
using Microsoft.Xna.Framework;

namespace max.stage.component
{
    /// <summary>
    /// Represents a collision on a CollisionLayer.
    /// </summary>
    public class CollisionComponent : GeometryComponent
    {
        #region Constants
        const string SER_SHAPE = "shape";
        #endregion

        /// <summary>
        /// The shape of the collision border.
        /// </summary>
        /// <value>Polygon</value>
        public virtual Polygon2D Border { get; set; }

        #region Constructors
        /// <summary>
        /// Empty constructor.
        /// </summary>
        public CollisionComponent()
        {

        }

        /// <summary>
        /// Create a collision with a polygon border.
        /// </summary>
        /// <param name="Border">Border</param>
        public CollisionComponent(Polygon2D Border)
        {
            this.Border = Border;
        }

        /// <summary>
        /// Creates an object from deserialized data.
        /// </summary>
        /// <param name="Data">Data</param>
        public CollisionComponent(MaxSerializedObject Data)
        {
            Deserialize(Data);
        }
        #endregion

        #region Geometry

        /// <summary>
        /// Determine whether a line intersects this collision.
        /// </summary>
        /// <param name="Line">Line</param>
        /// <returns>True/False</returns>
        public override bool LineIntersect(Line2D Line)
        {
            return Border.IntersectPoints(Line).Count > 0;
        }

        /// <summary>
        /// Gets the distance from the closest intersection with a line, relative
        /// to Point1.
        /// </summary>
        /// <param name="Line">Line</param>
        /// <returns>Distance</returns>
        public override float LineIntersectDistance(Line2D Line)
        {
            return Border.LineIntersectDistance(Line);
        }

        /// <summary>
        /// Distance of this collision to a point.
        /// </summary>
        /// <param name="Point">Point</param>
        /// <returns>Distance</returns>
        public override float DistanceToPoint(Vector2 Point)
        {
            return Vector2.Distance(Point, Border.Center);
        }

        #endregion

        /// <summary>
        /// Unused.
        /// </summary>
        /// <param name="obj">Object to compare</param>
        /// <returns>0</returns>
        public override int CompareTo(object obj)
        {
            return 0;
        }

        #region Serialize
        /// <summary>
        /// Serializes the data in the collision into a dictionary.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Data</returns>
        protected override MaxSerializedObject DoSerialize(MaxSerializedObject Data)
        {
            Data.AddSerializedObject(SER_SHAPE, Border);
            return Data;
        }

        /// <summary>
        /// Deserializes data into a collision.
        /// </summary>
        /// <param name="Data">Data</param>
        public override void Deserialize(MaxSerializedObject Data)
        {
            MaxSerializedObject obj = Data.GetSerializedObject(SER_SHAPE);
            obj.SetType(typeof(Polygon2D));
            Border = (Polygon2D)obj.Deserialize();
        }

        /// <summary>
        /// Creates an instance of this type of object from the provided data.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Deserialized new instance</returns>
        public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable a = new CollisionComponent();
            a.Deserialize(Data);
            return a;
        }

        #endregion
    }
}

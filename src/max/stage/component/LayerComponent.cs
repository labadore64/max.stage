﻿using max.serialize;
using max.stage.layer.template;

namespace max.stage.component
{
    /// <summary>
    /// This class represents a layer component.
    /// </summary>
    public abstract class LayerComponent : ILayerComponent
    {
        /// <summary>
        /// Whether or not this layer has been initialized.
        /// </summary>
        /// <value>True/False</value>
        public bool Initialized { get; protected set; }

        /// <summary>
        /// Whether or not this component is serialized when the parent's Serialize method is called.
        /// </summary>
        /// <value>True/False</value>
        public bool Serializable { get; set; } = true;

        /// <summary>
        /// Creates an empty LayerComponent.
        /// </summary>
        public LayerComponent()
        {

        }

        /// <summary>
        /// The parent layer.
        /// </summary>
        public virtual IStageLayer Parent { get; set; }

        /// <summary>
        /// The Stage Controller that this component belongs to.
        /// </summary>
        public virtual StageController StageController
        {
            get
            {
                return Parent.StageController;
            }
            set
            {

            }
        }

        /// <summary>
        /// Whether or not this layer is active or not.
        /// </summary>
        /// <value>True/False</value>
        public virtual bool Active { get; set; } = true;

        /// <summary>
        /// Whether or not this layer has been disposed of.
        /// </summary>
        /// <value>True/False</value>
        public bool Disposed { get; protected set; }

        /// <summary>
        /// Whether or not this layer is destroyed with the regular Clear command.
        /// </summary>
        /// <value>True/False</value>
        public bool Persistent { get; set; }

        /// <summary>
        /// Starts the initialization.
        /// </summary>
        /// <param name="StageLayer">Parent layer</param>
        public void DoInitialize(IStageLayer StageLayer)
        {
            if (!Initialized)
            {
                Parent = StageLayer;
                Initialize();
                Initialized = true;
            }
        }

        /// <summary>
        /// Initializes the Layer Component.
        /// </summary>
        public virtual void Initialize()
        {
            // is empty
        }

        /// <summary>
        /// Starts the update process.
        /// </summary>
        public void DoUpdate()
        {
            if(Initialized && !Disposed && Active)
            {
                Update();
            }
        }

        /// <summary>
        /// Updates the state of the Layer Component.
        /// </summary>
        public virtual void Update()
        {
            // is empty
        }

        /// <summary>
        /// Starts the disposal process.
        /// </summary>
        public void DoDispose()
        {
            if (!Disposed && Initialized)
            {
                Dispose();
                Disposed = true;
            }
        }

        /// <summary>
        /// Disposes the resources of the Layer Component.
        /// </summary>
        public virtual void Dispose()
        {
            // is empty
        }

        /// <summary>
        /// Serializes the component into a MaxSerializedObject.
        /// </summary>
        /// <returns>Data</returns>
        public MaxSerializedObject Serialize()
        {
            MaxSerializedObject Data = MaxSerializedObject.Create();
            if (Serializable)
            {
                Data = DoSerialize(Data);
                Data.SetType(GetType());
            }
            return Data;
        }

        /// <summary>
        /// Deserializes the MaxSerializedObject into a component.
        /// </summary>
        /// <param name="Data">Data</param>
        public abstract void Deserialize(MaxSerializedObject Data);

        /// <summary>
        /// Do the work of the deserialization.
        /// Usually you will only have to override this method if
        /// inheriting LayerComponent.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Data</returns>

        protected abstract MaxSerializedObject DoSerialize(MaxSerializedObject Data);

        /// <summary>
        /// Creates an instance of this type of object from the provided data.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Deserialized new instance</returns>
        public abstract IMaxSerializable CreateInstance(MaxSerializedObject Data);
    }
}

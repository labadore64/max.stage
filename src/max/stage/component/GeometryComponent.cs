﻿using max.geometry.shape2D;
using Microsoft.Xna.Framework;
using System;

namespace max.stage.component
{
    /// <summary>
    /// An abstract class used for the AXLayer Components.
    /// This allows them to have additional geometry and the ability
    /// to be compared.
    /// </summary>
    public abstract class GeometryComponent : LayerComponent, IComparable
    {
        #region Geometry Methods
        /// <summary>
        /// Determines whether or not a line intersects this map element.
        /// </summary>
        /// <param name="Line">Line</param>
        /// <returns>Intersection</returns>
        public abstract bool LineIntersect(Line2D Line);
        /// <summary>
        /// Get the distance of where the line intersects this map element.
        /// </summary>
        /// <param name="Line">Line</param>
        /// <returns>Distance</returns>
        public abstract float LineIntersectDistance(Line2D Line);
        /// <summary>
        /// The distance of this stage component to a point.
        /// </summary>
        /// <param name="Point">Point to test</param>
        /// <returns>Distance</returns>
        public abstract float DistanceToPoint(Vector2 Point);

        /// <summary>
        /// Compares this component to another object for sorting.
        /// </summary>
        /// <param name="obj">Comparing object</param>
        /// <returns>CompareTo result</returns>
        public abstract int CompareTo(object obj);
        #endregion
    }
}

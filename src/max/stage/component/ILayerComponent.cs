﻿using max.serialize;
using max.stage.layer.template;

namespace max.stage.component
{
    /// <summary>
    /// Represents an element on a layer.
    /// </summary>
    public interface ILayerComponent : IMaxSerializable
    {
        #region Properties

        /// <summary>
        /// Whether or not this layer has been initialized.
        /// </summary>
        /// <value>True/False</value>
        bool Initialized { get; }

        /// <summary>
        /// The parent stage layer.
        /// </summary>
        /// <value>Parent</value>
        IStageLayer Parent { get; set; }

        /// <summary>
        /// The Stage Controller that this component belongs to.
        /// </summary>
        /// <value>Stage Controller</value>
        StageController StageController { get; set; }

        /// <summary>
        /// Whether or not this stage component is active.
        /// </summary>
        /// <value>True/False</value>
        bool Active { get; set; }

        /// <summary>
        /// Whether or not this layer has been disposed of.
        /// </summary>
        /// <value>True/False</value>
        bool Disposed { get; }

        /// <summary>
        /// Whether or not this layer is disposed of when a Layer is cleared.
        /// If true, the component must be removed manually or with ClearAll().
        /// </summary>
        /// <value>True/False</value>
        bool Persistent { get; set; }
        #endregion

        #region Controller methods

        /// <summary>
        /// Initializes the stage component.
        /// </summary>
        void Initialize();

        /// <summary>
        /// Starts the initialization. Passes to Initialize(),
        /// which is overriden by child classes.
        /// </summary>
        /// <param name="StageLayer">The parent Stage Layer</param>
        void DoInitialize(IStageLayer StageLayer);

        /// <summary>
        /// Updates the stage component.
        /// </summary>
        void Update();

        /// <summary>
        /// Starts the update. Passes to Update(),
        /// which is overriden by child classes.
        /// </summary>
        void DoUpdate();

        /// <summary>
        /// Disposes of the stage component's contents.
        /// </summary>
        void Dispose();

        /// <summary>
        /// Starts the disposal. Passes to Dispose(),
        /// which is overriden by child classes.
        /// </summary>
        void DoDispose();
        #endregion
    }
}
